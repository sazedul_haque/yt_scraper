FROM python:3.8-slim-buster
RUN mkdir /usr/src/app/
WORKDIR /usr/src/app/

COPY . .

RUN apt-get update && apt-get install -y git

RUN pip install -r requirements.txt


RUN PYTHONPATH=. alembic upgrade head

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]

EXPOSE 8000
