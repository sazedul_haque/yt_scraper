# README #

### What is this repository for? ###

* For a test project

### Quick summary ###
1. Using Youtube API (https://developers.google.com/youtube/v3/) scrapes channel videos with tags and stats. Also you need to track changes of video stats every N minutes in order to see how videos are performing. Please pick the interval to scan stats which, according to you, is efficient and smart. You can hardcode channel ID in code, that’s not important.
2. Create DB scheme and save scraped data. Please consider, that we will want to scan a
lot of channels, so queries to aggregate and select data shouldn’t take long. Use any database you feel right.
3. Create mini API, where you can filter videos:
    1. By tags.
    2. By video performance (first hour views divided by channels all videos first hour views median)

Bonus points for:
  1. pseudo algorithm for fetching as many youtube channels as possible.
  2. unit tests
  
### Requirements: ###
* Python; (use Django/flask/FastAPI framework, please) 
* MySQL/POSTGRESQL DB;

To run this application you need to create a .env file by copying the data given in "env.example" file.

add your prefer database information in "DATABASE_URL" environment key
 
then run the following command to create database and table by migration 

```
(venv) mss:youtube_scraper ms$ PYTHONPATH=. alembic upgrade head
```
After migration Run the following command to start the application

```
 uvicorn main:app --reload
```
We can do a lot of improvement if we want.

#### Reason of choosing FastAPI ###

As its a short application and better to be async and there are lots of feature we can add here.For details read
visit (https://fastapi.tiangolo.com/).

I am strong in django and did lot of project with lots of challenges. If you want to know more details feel free to ask.

Thanks 

