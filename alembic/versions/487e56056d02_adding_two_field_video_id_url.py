"""adding two field video_id, url

Revision ID: 487e56056d02
Revises: 720018abef5b
Create Date: 2021-06-07 03:27:25.071343

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '487e56056d02'
down_revision = '720018abef5b'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_unique_constraint(None, 'youtube_base', ['id'])
    op.add_column('youtube_detail', sa.Column('video_id', sa.String(), nullable=True))
    op.add_column('youtube_detail', sa.Column('url', sa.String(), nullable=True))
    op.create_unique_constraint(None, 'youtube_detail', ['id'])
    op.drop_column('youtube_detail', 'channel_id')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('youtube_detail', sa.Column('channel_id', sa.INTEGER(), autoincrement=False, nullable=True))
    op.drop_constraint(None, 'youtube_detail', type_='unique')
    op.drop_column('youtube_detail', 'url')
    op.drop_column('youtube_detail', 'video_id')
    op.drop_constraint(None, 'youtube_base', type_='unique')
    # ### end Alembic commands ###
