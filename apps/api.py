from typing import Any

import fastapi
from sqlalchemy.orm import Session
from sqlalchemy import asc
from fastapi_pagination import Params
from fastapi_pagination.ext.sqlalchemy import paginate

from apps import schemas, models
from apps.services import YTStatistic
from core import deps
from core.config import settings

router = fastapi.APIRouter(
    prefix="/yt",
    tags=["youtube"],
    responses={404: {"description": "Not found"}},
)


@router.post("/search/", )
async def yt_search(
        req: schemas.SearchParams,
        db: Session = fastapi.Depends(deps.get_db),
) -> Any:
    params = {
        "part": req.part,
        "channelId": req.channel_id,
        "key": settings.YT_API_KEY,
        "type": "video",
        "maxResults": 50,
    }
    yt_state = YTStatistic()
    search_response = await yt_state.get(url="search", params=params)
    if search_response:
        yt_base = db.query(models.YoutubeBase).filter(models.YoutubeBase.channel_id == req.channel_id).first()

        if not yt_base:
            yt_base = models.YoutubeBase(
                channel_id=req.channel_id,
                part=req.part,
                etag=search_response.get('etag'),
                page_info_total_results=search_response.get('pageInfo').get("totalResults")
            )
        else:
            yt_base.part = req.part
            yt_base.page_info_total_results = search_response.get('pageInfo').get("totalResults")

        db.add(yt_base)
        db.commit()
    for item in search_response.get("items"):
        params = {
            "part": "statistics,snippet",
            "id": item.get("id").get("videoId"),
            "key": settings.YT_API_KEY,
            "maxResults": 100
        }
        response_detail = await yt_state.get(url="videos", params=params)
        if response_detail and 'items' in response_detail:
            response_detail = response_detail.get('items')[0]
            yt_detail = db.query(models.YoutubeDetail).filter(
                models.YoutubeDetail.video_id == response_detail.get('id')).first()

            if not yt_detail:
                yt_detail = models.YoutubeDetail(
                    video_id=response_detail.get('id'),
                    url=f"https://www.youtube.com/watch?v={response_detail.get('id')}",
                    statistics=response_detail.get('statistics'),
                    youtube_base_id=yt_base.id,
                )
            else:
                yt_detail.statistics = response_detail.get('statistics')

            db.add(yt_detail)
            db.commit()
            for tag in response_detail.get('snippet').get('tags'):
                tags = models.Tag(
                    youtube_detail_id=yt_detail.id,
                    name=tag
                )
                db.add(tags)
                db.commit()
    return yt_base


@router.get(
    "/filter/",
    response_model=schemas.Items
)
async def yt_filter(
        tags: str = None,
        db: Session = fastapi.Depends(deps.get_db),
        params: Params = fastapi.Depends()
) -> Any:
    query = db.query(models.Tag)

    if tags:
        query = query.filter(models.Tag.name == tags)
    else:
        query = query.order_by(asc(models.Tag.id))

    return paginate(query, params=params)


@router.post("/videos/", )
async def yt_videos(
        # db: Session = fastapi.Depends(get_db),
        req: schemas.VideoParams
) -> Any:
    """
    Retrieve youtube_scrapers.
    """
    params = {
        "part": req.part,
        "id": req.video_id,
        "key": settings.YT_API_KEY,
        "maxResults": 100
    }
    yt_state = YTStatistic()
    response = await yt_state.get(url="videos", params=params)
    # response = await yt_state.get(url="channels", params=params)
    return response


@router.post("/channels/", )
async def yt_channels(
        # db: Session = fastapi.Depends(get_db),
        req: schemas.Params
) -> Any:
    """
    Retrieve youtube_scrapers.
    """
    params = {
        "part": req.part,
        "id": req.channel_id,
        "key": settings.YT_API_KEY,
        "maxResults": 100
    }
    yt_state = YTStatistic()
    response = await yt_state.get(url="channels", params=params)
    # response = await yt_state.get(url="channels", params=params)
    return response
