from typing import TYPE_CHECKING

import sqlalchemy as sa
from sqlalchemy.orm import relationship

from core.models import BaseModel


class YoutubeBase(BaseModel):
    __tablename__ = "youtube_base"
    channel_id = sa.Column(sa.String, index=True)
    part = sa.Column(sa.String, unique=True, index=True, nullable=False)
    etag = sa.Column(sa.String, nullable=False)
    page_info_total_results = sa.Column(sa.String, nullable=False)
    details = relationship("YoutubeDetail", back_populates="youtube_base")


class YoutubeDetail(BaseModel):
    __tablename__ = "youtube_detail"
    title = sa.Column(sa.String, index=True)
    description = sa.Column(sa.String)
    video_id = sa.Column(sa.String)
    url = sa.Column(sa.String)
    statistics = sa.Column(sa.JSON)
    youtube_base_id = sa.Column(sa.Integer, sa.ForeignKey("youtube_base.id"))
    youtube_base = relationship("YoutubeBase", back_populates="details")
    tags = relationship("Tag", back_populates="youtube_details")
    statistic_histories = relationship("StatisticHistory", back_populates="youtube_details_1")


class Tag(BaseModel):
    __tablename__ = "tag"
    name = sa.Column(sa.String, index=True)
    description = sa.Column(sa.String)
    youtube_detail_id = sa.Column(sa.Integer, sa.ForeignKey("youtube_detail.id"))
    youtube_details = relationship("YoutubeDetail", back_populates="tags")
    # youtube_details = relationship("YoutubeDetail", uselist=False, back_populates="tags")


class StatisticHistory(BaseModel):
    __tablename__ = "statistic_history"
    view_count = sa.Column(sa.BigInteger, index=True)
    like_count = sa.Column(sa.Integer, index=True)
    disk_count = sa.Column(sa.Integer, index=True)
    favorite_count = sa.Column(sa.Integer, index=True)
    comment_count = sa.Column(sa.Integer, index=True)
    youtube_detail_id = sa.Column(sa.Integer, sa.ForeignKey("youtube_detail.id"))
    youtube_details_1 = relationship("YoutubeDetail", uselist=False, back_populates="statistic_histories")

