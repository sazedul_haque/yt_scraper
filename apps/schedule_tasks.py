import datetime
import logging
from apps import schemas, models

from apps.services import YTStatistic
from core.config import settings
from db.session import SessionLocal


async def startup_task(
        db=SessionLocal()
):
    logging.exception(f"time: {datetime.datetime.now()}")
    # ---------------------------------------------------------------
    # Initially we will get all the video id that specific to the channels
    # Details:-----------------------------------------
    # N.B. For now we are getting only 50 video of first page. Later
    # we can went through all the pages to get all the videos related the channels
    # ---------------------------------------------------------------

    params = {
        "part": "id",
        "id": settings.CHANNEL_ID,
        "key": settings.YT_API_KEY,
        "maxResults": 50
    }
    yt_state = YTStatistic()
    search_response = await yt_state.get(url="search", params=params)
    for item in search_response.get("items"):
        params = {
            "part": "statistics,snippet",
            "id": item.get("id").get("videoId"),
            "key": settings.YT_API_KEY,
            "maxResults": 100
        }
        response_detail = await yt_state.get(url="videos", params=params)
        if response_detail and 'items' in response_detail and len(response_detail.get('items')) > 0:
            response_detail = response_detail.get('items')[0]
            stat = response_detail.get("statistics")
            yt_detail = db.query(models.YoutubeDetail).filter(
                models.YoutubeDetail.video_id == response_detail.get("id")
            ).first()
            if not yt_detail:
                yt_detail = models.YoutubeDetail(
                    video_id=response_detail.get('id'),
                    url=f"https://www.youtube.com/watch?v={response_detail.get('id')}",
                    statistics=stat
                )
            else:
                yt_detail.statistics = response_detail.get('statistics')

            db.add(yt_detail)
            db.commit()
            for tag in response_detail.get('snippet').get('tags'):
                tags = models.Tag(
                    youtube_detail_id=yt_detail.id,
                    name=tag
                )
                db.add(tags)
                db.commit()
            state_details = db.query(models.StatisticHistory).filter(
                models.StatisticHistory.youtube_detail_id == yt_detail.id,
                models.StatisticHistory.view_count == int(stat.get("viewCount")),
                models.StatisticHistory.like_count == int(stat.get("likeCount")),
                models.StatisticHistory.disk_count == int(stat.get("dislikeCount")),
                models.StatisticHistory.favorite_count == int(stat.get("favoriteCount")),
                models.StatisticHistory.comment_count == int(stat.get("commentCount")),
            ).first()
            if not state_details:
                state_details = models.StatisticHistory(
                    view_count=stat.get("viewCount"),
                    like_count=stat.get("likeCount"),
                    disk_count=stat.get("dislikeCount"),
                    favorite_count=stat.get("favoriteCount"),
                    comment_count=stat.get("commentCount"),
                    youtube_detail_id=yt_detail.id,
                )
                db.add(state_details)
                db.commit()

                print(f"NewChange came for Video ID ={yt_detail.video_id}, id = {yt_detail.id}", response_detail.get("statistics"))
