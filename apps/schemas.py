import uuid

from pydantic import BaseModel
from typing import Optional, List


class SearchParams(BaseModel):
    channel_id: Optional[str] = 'UC2i8njKuW6P3OnBMfgFt68g'
    part: Optional[str] = 'id,snippet'
    page_token: Optional[str] = None


class Params(BaseModel):
    channel_id: Optional[str] = 'UC2i8njKuW6P3OnBMfgFt68g'
    part: Optional[str] = 'statistics,snippet'


class VideoParams(BaseModel):
    video_id: Optional[str] = 'UC2i8njKuW6P3OnBMfgFt68g'
    part: Optional[str] = 'statistics,snippet'


class StatisticHistory(BaseModel):
    view_count: Optional[int] = 0
    like_count: Optional[int] = 0
    disk_count: Optional[int] = 0
    favorite_count: Optional[int] = 0
    comment_count: Optional[int] = 0

    class Config:
        orm_mode = True

class YoutubeDetails(BaseModel):
    title: Optional[str] = ""
    description: Optional[str] = ""
    video_id: str
    url: str
    statistics: Optional[dict] = ""
    youtube_base_id: int
    statistic_histories: List[StatisticHistory]

    class Config:
        orm_mode = True


class YoutubeTags(BaseModel):
    name: str
    youtube_detail_id: int
    youtube_details: YoutubeDetails

    class Config:
        orm_mode = True


class Items(BaseModel):
    items: List[YoutubeTags]
    total: int
    page: int
    size: int

    class Config:
        orm_mode = True
