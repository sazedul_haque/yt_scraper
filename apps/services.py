# -*- coding: utf-8 -*-

# Sample Python code for youtube.channels.list
# See instructions for running these code samples locally:
# https://developers.google.com/explorer-help/guides/code_samples#python

import os

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors
import httpx
from pydantic import typing

from core.config import settings

scopes = ["https://www.googleapis.com/auth/youtube.readonly"]

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
file_location = os.path.join(BASE_DIR, "core",
                             "client_secret_59209014574-chks70270p20g68unq8qsdo5gtecse1l.apps.googleusercontent.com.json")


class YTStatistic(object):
    def __init__(self):
        # self.YT_API_KEY = settings.YT_API_KEY
        self.YT_API_URL = settings.YT_API_URL
        # self.channel_id = channel_id

    async def get(
            self,
            url: str,
            params: typing.Optional[typing.Dict] = None
    ) -> dict:
        """

        :param url:
        :param params:
        :return: Response
        """

        headers = {}

        async with httpx.AsyncClient() as client:
            response = await client.get(
                url=f"{self.YT_API_URL}{url}",
                headers=headers,
                params=params,
                timeout=None
            )

        if response.status_code == 200:
            return response.json()
        else:
            return {
                "success": False,
                "message": f"{response.text}",
            }

    async def post(
            self,
            url: str,
            payload: typing.Optional[typing.Dict] = None
    ) -> dict:
        """

        :param url:
        :param params:
        :return:
        """
        headers = {}

        async with httpx.AsyncClient() as client:
            response = await client.post(
                url=f"{self.YT_API_URL}{url}",
                headers=headers,
                json=payload,
                timeout=None
            )

        if response.status_code == 200:
            return response.json()
        else:
            return {
                "success": False,
                "message": f"{response.text}",
            }
