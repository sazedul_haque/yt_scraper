from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from core.config import settings
from auth.tests.utils.youtube_scraper import create_random_youtube_scraper


def test_create_youtube_scraper(
    client: TestClient, superuser_token_headers: dict, db: Session
) -> None:
    data = {"title": "Foo", "description": "Fighters"}
    response = client.post(
        f"{settings.API_V1_STR}/youtube_scrapers/", headers=superuser_token_headers, json=data,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["title"] == data["title"]
    assert content["description"] == data["description"]
    assert "id" in content
    assert "owner_id" in content


def test_read_youtube_scraper(
    client: TestClient, superuser_token_headers: dict, db: Session
) -> None:
    youtube_scraper = create_random_youtube_scraper(db)
    response = client.get(
        f"{settings.API_V1_STR}/youtube_scrapers/{youtube_scraper.id}", headers=superuser_token_headers,
    )
    assert response.status_code == 200
    content = response.json()
    assert content["title"] == youtube_scraper.title
    assert content["description"] == youtube_scraper.description
    assert content["id"] == youtube_scraper.id
    assert content["owner_id"] == youtube_scraper.owner_id
