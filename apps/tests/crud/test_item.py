from sqlalchemy.orm import Session

from auth import crud
from auth.schemas.youtube_scraper import FoundationCreate, FoundationUpdate
from auth.tests.utils.user import create_random_user
from auth.tests.utils.utils import random_lower_string


def test_create_youtube_scraper(db: Session) -> None:
    title = random_lower_string()
    description = random_lower_string()
    youtube_scraper_in = FoundationCreate(title=title, description=description)
    user = create_random_user(db)
    youtube_scraper = crud.youtube_scraper.create_with_user(db=db, obj_in=youtube_scraper_in, owner_id=user.id)
    assert youtube_scraper.title == title
    assert youtube_scraper.description == description
    assert youtube_scraper.owner_id == user.id


def test_get_youtube_scraper(db: Session) -> None:
    title = random_lower_string()
    description = random_lower_string()
    youtube_scraper_in = FoundationCreate(title=title, description=description)
    user = create_random_user(db)
    youtube_scraper = crud.youtube_scraper.create_with_user(db=db, obj_in=youtube_scraper_in, owner_id=user.id)
    stored_youtube_scraper = crud.youtube_scraper.get(db=db, id=youtube_scraper.id)
    assert stored_youtube_scraper
    assert youtube_scraper.id == stored_youtube_scraper.id
    assert youtube_scraper.title == stored_youtube_scraper.title
    assert youtube_scraper.description == stored_youtube_scraper.description
    assert youtube_scraper.owner_id == stored_youtube_scraper.owner_id


def test_update_youtube_scraper(db: Session) -> None:
    title = random_lower_string()
    description = random_lower_string()
    youtube_scraper_in = FoundationCreate(title=title, description=description)
    user = create_random_user(db)
    youtube_scraper = crud.youtube_scraper.create_with_user(db=db, obj_in=youtube_scraper_in, owner_id=user.id)
    description2 = random_lower_string()
    youtube_scraper_update = FoundationUpdate(description=description2)
    youtube_scraper2 = crud.youtube_scraper.update(db=db, db_obj=youtube_scraper, obj_in=youtube_scraper_update)
    assert youtube_scraper.id == youtube_scraper2.id
    assert youtube_scraper.title == youtube_scraper2.title
    assert youtube_scraper2.description == description2
    assert youtube_scraper.owner_id == youtube_scraper2.owner_id


def test_delete_youtube_scraper(db: Session) -> None:
    title = random_lower_string()
    description = random_lower_string()
    youtube_scraper_in = FoundationCreate(title=title, description=description)
    user = create_random_user(db)
    youtube_scraper = crud.youtube_scraper.create_with_user(db=db, obj_in=youtube_scraper_in, owner_id=user.id)
    youtube_scraper2 = crud.youtube_scraper.remove(db=db, id=youtube_scraper.id)
    youtube_scraper3 = crud.youtube_scraper.get(db=db, id=youtube_scraper.id)
    assert youtube_scraper3 is None
    assert youtube_scraper2.id == youtube_scraper.id
    assert youtube_scraper2.title == title
    assert youtube_scraper2.description == description
    assert youtube_scraper2.owner_id == user.id
