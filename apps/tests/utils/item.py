from typing import Optional

from sqlalchemy.orm import Session

from auth import crud, models
from auth.schemas.youtube_scraper import FoundationCreate
from auth.tests.utils.user import create_random_user
from auth.tests.utils.utils import random_lower_string


def create_random_youtube_scraper(db: Session, *, owner_id: Optional[int] = None) -> models.Foundation:
    if owner_id is None:
        user = create_random_user(db)
        owner_id = user.id
    title = random_lower_string()
    description = random_lower_string()
    youtube_scraper_in = FoundationCreate(title=title, description=description, id=id)
    return crud.youtube_scraper.create_with_user(db=db, obj_in=youtube_scraper_in, owner_id=owner_id)
