# User types
ADMIN_ID, MANAGER_ID, STAFF_ID, USER_ID = 1, 2, 3, 4
ADMIN, MANAGER, STAFF, USER = 'Admin', 'Manager', 'Staff', 'User'
USER_TYPES = (
    (ADMIN_ID, ADMIN),
    (MANAGER_ID, MANAGER),
    (STAFF_ID, STAFF),
    (USER_ID, USER)
)