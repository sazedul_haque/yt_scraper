import uuid
import datetime

from sqlalchemy import Column, Integer, DateTime
from sqlalchemy_utils import UUIDType

from db.base_class import Base


class BaseModel(Base):
    __tablename__ = "base_model"
    __abstract__ = True

    id = Column(Integer, autoincrement=True, unique=True, primary_key=True)
    uid = Column(UUIDType(binary=False), unique=True, default=uuid.uuid4)
    created_at = Column(DateTime, default=datetime.datetime.now)
    updated_at = Column(DateTime, nullable=True, default=datetime.datetime.now)
