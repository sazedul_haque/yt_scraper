# Import all the models, so that Base has them before being
# imported by Alembic
from sqlalchemy.orm import declarative_base

from db.base_class import Base  # noqa
from apps.models import YoutubeBase, YoutubeDetail, Tag, StatisticHistory
